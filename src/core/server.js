import express from 'express';

const server = express();
const port = process.env.SERVER_PORT;

server.get('/', (req, res) => res.send('Hello world'));
server.listen(port, () => console.info(`Server is listening at port ${port}`));