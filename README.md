<h1 align="center">Message API ✉</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1.0-blue.svg?cacheSeconds=2592000" />
</p>

Instant messaging API

## Install
```sh
npm run build
```

## Usage
```sh
npm run start
```

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/fabien-renaud/message-api/issues).

## Show your support

Give a ⭐️ if this project helped you!
